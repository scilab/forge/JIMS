/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2011 - Allan CORNET
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "OptionsHelper.h"
/*--------------------------------------------------------------------------*/
static char copyOccured = 0;
static char methodOfConv = 0;
static char allowReload = 0;
static char autoUnwrap = 0;
/*--------------------------------------------------------------------------*/
void setMethodOfConv(const char _methodOfConv)
{
    methodOfConv = _methodOfConv;
}
/*--------------------------------------------------------------------------*/
void setAllowReload(const char _allowReload)
{
    allowReload = _allowReload;
}
/*--------------------------------------------------------------------------*/
void setAutoUnwrap(const char _autoUnwrap)
{
    autoUnwrap = _autoUnwrap;
}
/*--------------------------------------------------------------------------*/
const char getMethodOfConv(void)
{
    return methodOfConv;
}
/*--------------------------------------------------------------------------*/
const char getAllowReload(void)
{
    return allowReload;
}
/*--------------------------------------------------------------------------*/
const char getAutoUnwrap(void)
{
    return autoUnwrap;
}
/*--------------------------------------------------------------------------*/
const char getCopyOccured(void)
{
    return copyOccured;
}
/*--------------------------------------------------------------------------*/
void setCopyOccured(const char _copyOccured)
{
    copyOccured = _copyOccured;
}
/*--------------------------------------------------------------------------*/
