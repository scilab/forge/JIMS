/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "ScilabObjects.h"
#include "JIMS.h"
#define __USE_DEPRECATED_STACK_FUNCTIONS__
#include "api_scilab.h"
#include "stack-c.h"
#include "Scierror.h"
#include "getSciArgs.h"
#include "OptionsHelper.h"
#include "call_scilab.h"
/*--------------------------------------------------------------------------*/
#define _INVOKE_ "!!_jinvoke_"
#define COPYINVOCATIONMACROTOSTACK "copyInvocationMacroToStack"
/*--------------------------------------------------------------------------*/

extern int C2F(varfunptr)(int *, int *,int *);

static char isInit = 0;
void initialization()
{
    if (!isInit)
    {
        int* mlistaddr = NULL;
        const char* fields[] = {"_JVoid"};
        SciErr err;

        createNamedJavaObject(_JOBJ, "jnull", 0);

        err = createNamedMList(pvApiCtx, "jvoid", 1, &mlistaddr);
        if (err.iErr)
        {
            printError(&err, 0);
            return;
        }

        err = createMatrixOfStringInNamedList(pvApiCtx, "jvoid", mlistaddr, 1, 1, 1, fields);
        if (err.iErr)
        {
            printError(&err, 0);
            return;
        }

        isInit = 1;
    }
}

/**
 * Create a named Java Object (_JObj or _JClass) on the stack
 */
int createNamedJavaObject(int type, const char *name, int id)
{
    const char **fields;
    int* mlistaddr = NULL;
    SciErr err;

    switch (type)
    {
    case _JOBJ:;
        fields = _JObj;
        break;
    case _JCLASS:;
        fields = _JClass;
        break;
    default :;
        fields = _JObj;
        break;
    }

    err = createNamedMList(pvApiCtx, name, 2, &mlistaddr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    err = createMatrixOfStringInNamedList(pvApiCtx, name, mlistaddr, 1, 1, 2, fields);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    err = createMatrixOfInteger32InNamedList(pvApiCtx, name, mlistaddr, 2, 1, 1, &id);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    return 1;
}
/*--------------------------------------------------------------------------*/
/**
 * Create a Java Object at the given position on the stack
 */
int createJavaObjectAtPos(int type, int pos, int id)
{
    const char **fields = NULL;
    int* mlistaddr = NULL;
    SciErr err;

    switch (type)
    {
    case _JOBJ:;
        fields = _JObj;
        break;
    case _JCLASS:;
        fields = _JClass;
        break;
    default :;
        fields = _JObj;
        break;
    }

    err = createMList(pvApiCtx, pos, 2, &mlistaddr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    err = createMatrixOfStringInList(pvApiCtx, pos, mlistaddr, 1, 1, 2, fields);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    err = createMatrixOfInteger32InList(pvApiCtx, pos, mlistaddr, 2, 1, 1, &id);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    return 1;
}
/*--------------------------------------------------------------------------*/
/**
 * When obj.method(...) is invoked, the extraction of 'method' field is followed by a copy
 * of the macro !_invoke_, so obj.method(...) would be equivalent to !_invoke_(...)
 */
void copyInvocationMacroToStack(int pos)
{
    static int init = 0;
    static int id[nsiz];
    static int interf = 0;
    static int funnumber = 0;
    int tops = 0;
    
    if (!init)
    {
      int fins = 0;
      int funs = 0;
	init = 1;
	C2F(str2name)(_INVOKE_, id, strlen(_INVOKE_));
	fins = Fin;
	funs = C2F(com).fun;
	Fin = -1;
	C2F(funs)(id);
	funnumber = Fin;
	interf = C2F(com).fun;
	C2F(com).fun = funs;
	Fin = fins;
    }
    
    tops = Top;
    Top = Top - Rhs + pos - 1;
    
    // Create a function pointer variable
    C2F(varfunptr)(id, &interf, &funnumber);
    
    Top = tops;
    setCopyOccured(1);
}
/*--------------------------------------------------------------------------*/
void removeTemporaryVars(int *tmpvar)
{
    int i = 1;
    for (; i <= *tmpvar; i++)
    {
        removescilabjavaobject(tmpvar[i]);
    }
}
/*--------------------------------------------------------------------------*/
int removeVar(const char *fname, int *addr, int pos)
{
    SciErr err;
    int type, row, col, *id;

    err = getVarType(pvApiCtx, addr, &type);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    if (type == sci_mlist && (isJObj(addr) || isJClass(addr)))
    {
        err = getMatrixOfInteger32InList(pvApiCtx, addr, 2, &row, &col, &id);
        if (err.iErr)
        {
            printError(&err, 0);
            return 0;
        }

        removescilabjavaobject(*id);
        *id = 0;

        return 1;
    }
    else if (type == sci_strings)
    {
        char *varName = NULL;
        if (getAllocatedSingleString(pvApiCtx, addr, &varName))
        {
            return 0;
        }

        err = getVarAddressFromName(pvApiCtx, varName, &addr);
        if (err.iErr)
        {
            printError(&err, 0);
            return 0;
        }

        err = getVarType(pvApiCtx, addr, &type);
        if (err.iErr)
        {
            printError(&err, 0);
            return 0;
        }

        if (type == sci_mlist && (isJObj(addr) || isJClass(addr)))
        {
            err = getMatrixOfInteger32InList(pvApiCtx, addr, 2, &row, &col, &id);
            if (err.iErr)
            {
                printError(&err, 0);
                return 0;
            }

            removescilabjavaobject(*id);
            *id = 0;

            return 1;
        }
    }

    Scierror(999, "%s: Wrong type for input argument %i: a _JObj or a _JClass expected", fname, pos);
    return 0;
}
/*--------------------------------------------------------------------------*/
/**
 * Unwrap !
 */
int unwrap(int idObj, int pos)
{
    char *errmsg = NULL;
    int type = 0;

    if (idObj == 0) // null object
    {
        double *arr = NULL;
        SciErr err = allocMatrixOfDouble(pvApiCtx, pos, 0, 0, &arr);
        if (err.iErr)
        {
            printError(&err, 0);
            return 0;
        }
        return 1;
    }


    type = isunwrappable(idObj, &errmsg);
    if (errmsg)
    {
        Scierror(999,"%s: %s\n", "unwrap", errmsg);
        return 0;
    }

    switch (type)
    {
    case -1:;
        return 0;
    case 0:;
        unwrapdouble(idObj, pos, &errmsg);
        break;
    case 1:;
        unwraprowdouble(idObj, pos, &errmsg);
        break;
    case 2:;
        unwrapmatdouble(idObj, pos, &errmsg);
        break;
    case 3:;
        unwrapstring(idObj, pos, &errmsg);
        break;
    case 4:;
        unwraprowstring(idObj, pos, &errmsg);
        break;
    case 5:;
        unwrapmatstring(idObj, pos, &errmsg);
        break;
    case 6:;
        unwrapint(idObj, pos, &errmsg);
        break;
    case 7:;
        unwraprowint(idObj, pos, &errmsg);
        break;
    case 8:;
        unwrapmatint(idObj, pos, &errmsg);
        break;
    case 9:;
        unwrapboolean(idObj, pos, &errmsg);
        break;
    case 10:;
        unwraprowboolean(idObj, pos, &errmsg);
        break;
    case 11:;
        unwrapmatboolean(idObj, pos, &errmsg);
        break;
    case 12:;
        unwrapbyte(idObj, pos, &errmsg);
        break;
    case 13:;
        unwraprowbyte(idObj, pos, &errmsg);
        break;
    case 14:;
        unwrapmatbyte(idObj, pos, &errmsg);
        break;
    case 15:;
        unwrapshort(idObj, pos, &errmsg);
        break;
    case 16:;
        unwraprowshort(idObj, pos, &errmsg);
        break;
    case 17:;
        unwrapmatshort(idObj, pos, &errmsg);
        break;
    case 18:;
        unwrapchar(idObj, pos, &errmsg);
        break;
    case 19:;
        unwraprowchar(idObj, pos, &errmsg);
        break;
    case 20:;
        unwrapmatchar(idObj, pos, &errmsg);
        break;
    case 21:;
        unwrapfloat(idObj, pos, &errmsg);
        break;
    case 22:;
        unwraprowfloat(idObj, pos, &errmsg);
        break;
    case 23:;
        unwrapmatfloat(idObj, pos, &errmsg);
        break;
    case 24:;
        unwraplong(idObj, pos, &errmsg);
        break;
    case 25:;
        unwraprowlong(idObj, pos, &errmsg);
        break;
    case 26:;
        unwrapmatlong(idObj, pos, &errmsg);
        break;
    case 27:;
        unwrapdouble(idObj, pos, &errmsg);
        break;
    case 28:;
        unwrapint(idObj, pos, &errmsg);
        break;
    case 29:;
        unwraplong(idObj, pos, &errmsg);
        break;
    case 30:;
        unwrapbyte(idObj, pos, &errmsg);
        break;
    case 31:;
        unwrapchar(idObj, pos, &errmsg);
        break;
    case 32:;
        unwrapboolean(idObj, pos, &errmsg);
        break;
    case 33:;
        unwrapfloat(idObj, pos, &errmsg);
        break;
    case 34:;
        unwrapshort(idObj, pos, &errmsg);
        break;
    default:;
        return 0;
    }

    if (errmsg)
    {
        Scierror(999,"%s: %s\n", "unwrap", errmsg);
        return 0;
    }

    return 1;
}
/*--------------------------------------------------------------------------*/
