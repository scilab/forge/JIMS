// ====================================================================
// Allan CORNET - DIGITEO - 2011
// This file is released into the public domain
// ====================================================================
function builder_src()
  tbx_builder_src_lang(["cpp", "c"],get_absolute_file_path("builder_src.sce"));
endfunction
builder_src();
clear builder_src;
