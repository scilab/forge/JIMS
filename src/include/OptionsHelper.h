/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2011 - Allan CORNET
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __OPTIONSHELPER_H__
#define __OPTIONSHELPER_H__
/*--------------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C"
{
#endif


void setMethodOfConv(const char _methodOfConv);
void setAllowReload(const char _allowReload);
void setAutoUnwrap(const char _autoUnwrap);
void setCopyOccured(const char _copyOccured);

const char getMethodOfConv(void);
const char getAllowReload(void);
const char getAutoUnwrap(void);
const char getCopyOccured(void);

#ifdef __cplusplus
};
#endif /* extern "C" */
/*--------------------------------------------------------------------------*/
#endif /* __OPTIONSHELPER_H__ */
