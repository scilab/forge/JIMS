/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __SCILABOBJECTS_H__
#define __SCILABOBJECTS_H__
/*--------------------------------------------------------------------------*/
#ifdef _MSC_VER
#include <windows.h>
#endif

#ifndef _MSC_VER /* Defined anyway with Visual */
#if !defined(byte)
typedef signed char byte;
#else
#pragma message("Byte has been redefined elsewhere. Some problems can happen")
#endif
#endif
/* ------------------------------------------------------------------------- */
static int ZERO = 0;
static int ONE = 1;
static int TWO = 2;
static int THREE = 3;
/* ------------------------------------------------------------------------- */
void initialization();
int createNamedJavaObject(int type, const char *name, int id);
void copyInvocationMacroToStack(int pos);
int removeVar(const char *fname, int *addr, int pos);
int createJavaObjectAtPos(int type, int pos, int id);
void removeTemporaryVars(int *tmpvar);
void initscilabjavaobject(char**);
void enabletrace(char*, char**);
void disabletrace();
void garbagecollect(char**);
int loadjavaclass(char*, char, char**);
int createjavaarray(char*, int*, int, char**);
int newinstance(int, int*, int, char**);
int invoke(int, char*, int*, int, char**);
void setfield(int, char*, int, char**);
int getfield(int, char*, char**);
int getfieldtype(int, char*, char**);
char* getclassname(int, char**);
int getarrayelement(int, int *, int, char**);
void setarrayelement(int, int *, int, int, char**);
int javacast(int, char*, char**);
int javacastwithid(int, int, char**);
char* getrepresentation(int, char**);
int isvalidjavaobject(int);
void removescilabjavaobject(int);
void getaccessiblemethods(int, int, char**);
void getaccessiblefields(int, int, char**);
int wrapSingleDouble(double);
int wrapRowDouble(double*, int);
int wrapMatDouble(double*, int, int);
int wrapSingleInt(int);
int wrapRowInt(int*, int);
int wrapMatInt(int*, int, int);
int wrapSingleUInt(unsigned int);
int wrapRowUInt(unsigned int*, int);
int wrapMatUInt(unsigned int*, int, int);
int wrapSingleByte(byte);
int wrapRowByte(byte*, int);
int wrapMatByte(byte*, int, int);
int wrapSingleUByte(unsigned char);
int wrapRowUByte(unsigned char*, int);
int wrapMatUByte(unsigned char*, int, int);
int wrapSingleShort(short);
int wrapRowShort(short*, int);
int wrapMatShort(short*, int, int);
int wrapSingleUShort(unsigned short);
int wrapRowUShort(unsigned short*, int);
int wrapMatUShort(unsigned short*, int, int);
int wrapSingleString(char*);
int wrapRowString(char**, int);
int wrapMatString(char**, int, int);
int wrapSingleBoolean(int);
int wrapRowBoolean(int*, int);
int wrapMatBoolean(int*, int, int);
int wrapSingleChar(unsigned short);
int wrapRowChar(unsigned short*, int);
int wrapMatChar(unsigned short*, int, int);
int wrapSingleFloat(double);
int wrapRowFloat(double*, int);
int wrapMatFloat(double*, int, int);

#ifdef __SCILAB_INT64__
int wrapSingleLong(long long);
int wrapRowLong(long long*, int);
int wrapMatLong(long long*, int, int);
#endif

void* wrapAsDirectDoubleBuffer(double*, long, int*);
void* wrapAsDirectLongBuffer(long*, long, int*);
void* wrapAsDirectIntBuffer(int*, long, int*);
void* wrapAsDirectShortBuffer(short*, long, int*);
void* wrapAsDirectByteBuffer(byte*, long, int*);
void* wrapAsDirectCharBuffer(char*, long, int*);
void* wrapAsDirectFloatBuffer(float*, long, int*);
void releasedirectbuffer(void**, int*, int, char**);

int compilecode(char*, char**, int, char**);

int unwrap(int idObj, int pos);
void unwrapdouble(int, int, char**);
void unwraprowdouble(int, int, char**);
void unwrapmatdouble(int, int, char**);
void unwrapbyte(int, int, char**);
void unwraprowbyte(int, int, char**);
void unwrapmatbyte(int, int, char**);
void unwrapshort(int, int, char**);
void unwraprowshort(int, int, char**);
void unwrapmatshort(int, int, char**);
void unwrapint(int, int, char**);
void unwraprowint(int, int, char**);
void unwrapmatint(int, int, char**);
void unwrapboolean(int, int, char**);
void unwraprowboolean(int, int, char**);
void unwrapmatboolean(int, int, char**);
void unwrapstring(int, int, char**);
void unwraprowstring(int, int, char**);
void unwrapmatstring(int, int, char**);
void unwrapchar(int, int, char**);
void unwraprowchar(int, int, char**);
void unwrapmatchar(int, int, char**);
void unwrapfloat(int, int, char**);
void unwraprowfloat(int, int, char**);
void unwrapmatfloat(int, int, char**);
void unwraplong(int, int, char**);
void unwraprowlong(int, int, char**);
void unwrapmatlong(int, int, char**);
int isunwrappable(int, char**);

/*--------------------------------------------------------------------------*/
#endif /* __SCILABOBJECTS_H__ */
