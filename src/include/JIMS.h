/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#ifndef __JIMS_H__
#define __JIMS_H__

#define _JOBJ 0
#define _JCLASS 1

#define JAVAERROR "%s: An error occurred during the data retrieving in Java:\n%s\n"

static const char* _JClass[] = {"_JClass", "_id"};
static const char* _JObj[] = {"_JObj", "_id"};

#endif /* __JIMS_H__ */
/*--------------------------------------------------------------------------*/
