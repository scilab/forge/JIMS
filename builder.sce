// Copyright (C) 2008 - INRIA
// Copyright (C) 2009 - DIGITEO

// This file is released into the public domain
// =============================================================================
mode(-1);
lines(0);
// =============================================================================
function builder_main()

  TOOLBOX_NAME  = "JIMS";
  TOOLBOX_TITLE = "JIMS";
  toolbox_dir   = get_absolute_file_path("builder.sce");

// Check Scilab's version
// =============================================================================

  try
    v = getversion("scilab");
  catch
    error(gettext("Scilab 5.3 or more is required."));
  end

  if v(2) < 3 then
    // new API in scilab 5.3
    error(gettext('Scilab 5.3 or more is required.'));  
  end

// Check modules_manager module availability
// =============================================================================
  if ~isdef('tbx_build_loader') then
    error(msprintf(gettext('%s module not installed."), 'modules_manager'));
  end


  if getos() == 'Windows' then
    if ~isdir(SCI + '\java\jdk') then
      error("it requires a local built of Scilab");
    end
    PATH = getenv('PATH');
    JAVA_HOME = SCI + '\java\jdk';
    JDK_PATH = SCI + '\java\jdk';
    JRE_PATH = SCI + '\java\jre';
    ANT_PATH = SCI + '\java\ant'

    setenv('JAVA_HOME', JAVA_HOME);
    setenv('JDK_PATH', JDK_PATH);
    setenv('JRE_PATH', JRE_PATH);
    setenv('ANT_PATH', ANT_PATH);
    setenv('PATH',ANT_PATH + '\bin;' + JDK_PATH + ';' + JRE_PATH + ';'+ PATH);
  end

  // Compile Java
  unix_w('ant all');

  tbx_builder_macros(toolbox_dir);
  tbx_builder_src(toolbox_dir);
  tbx_builder_gateway(toolbox_dir);
  tbx_builder_help(toolbox_dir);
  tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
  tbx_build_cleaner(TOOLBOX_NAME, toolbox_dir);
endfunction
// =============================================================================
builder_main()
clear builder_main;
// =============================================================================