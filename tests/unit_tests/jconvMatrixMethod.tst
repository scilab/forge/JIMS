//
//  Copyright (C) 2011 - DIGITEO - Allan CORNET
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
// <-- JVM MANDATORY -->
//

if execstr("jconvMatrixMethod(""toto"")", "errcatch") <> 999 then pause, end
if execstr("jconvMatrixMethod(1)", "errcatch") <> 999 then pause, end

method = jconvMatrixMethod();
if method <> "cr" then pause, end

jconvMatrixMethod("rc");
method = jconvMatrixMethod();
if method <> "rc" then pause, end