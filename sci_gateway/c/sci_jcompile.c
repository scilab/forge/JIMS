/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#define __USE_DEPRECATED_STACK_FUNCTIONS__
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "getSciArgs.h"
#include "localization.h"
#include "gw_helper.h"
/*--------------------------------------------------------------------------*/
int sci_jcompile(char *fname)
{
    SciErr err;
    int row = 0, col = 0;
    int *addr = NULL;
    char *className = NULL;
    char **code = NULL;
    char *errmsg = NULL;
    int ret = 0;
    int iType2 = 0;

    CheckRhs(2, 2);

    setCopyOccured(0);
    initialization();
    setIsNew(0);

    className = getSingleString(1, fname);
    if (!className)
    {
        return 0;
    }

    err = getVarAddressFromPosition(pvApiCtx, 2, &addr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    err = getVarType(pvApiCtx, addr, &iType2);
    if(err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    if (iType2 != sci_strings)
    {
        Scierror(999,_("%s: Wrong type for input argument #%d: string expected.\n"), fname, 2);
        return 0;
    }

    err = getVarDimension(pvApiCtx, addr, &row, &col);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    if ((row < 1 || col != 1) && (col < 1 || row != 1))
    {
        Scierror(999, "%s: Wrong argument size at position %i: one column or one row expected\n", fname, 2);
        return 0;
    }

    if (getAllocatedMatrixOfString(pvApiCtx, addr, &row, &col, &code))
    {
        return -1;
    }

    ret = compilecode(className, code, row != 1 ? row : col, &errmsg);
    freeAllocatedSingleString(className);
    freeAllocatedMatrixOfString(row, col, code);

    if (errmsg)
    {
        Scierror(999, JAVAERROR, fname, errmsg);
        FREE(errmsg);
        return 0;
    }

    if (!createJavaObjectAtPos(_JCLASS, Rhs + 1, ret))
    {
        removescilabjavaobject(ret);
        return 0;
    }

    LhsVar(1) = Rhs + 1;
    PutLhsVar();
    return 0;
}
/*--------------------------------------------------------------------------*/
