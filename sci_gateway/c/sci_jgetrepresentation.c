/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "gw_helper.h"
#include "ScilabObjects.h"
#define __USE_DEPRECATED_STACK_FUNCTIONS__
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "getSciArgs.h"
#include "MALLOC.h"
/*--------------------------------------------------------------------------*/
int sci_jgetrepresentation(char *fname)
{
    SciErr err;
    int tmpvar[2] = {0, 0};
    int *addr = NULL;
    int idObj = 0;
    char *errmsg = NULL;
    char* rep = NULL;

    CheckRhs(1, 1);

    setCopyOccured(0);
    initialization();
    setIsNew(0);

    err = getVarAddressFromPosition(pvApiCtx, 1, &addr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    idObj = getIdOfArg(addr, fname, tmpvar, 0, 1);

    if (idObj == -1)
    {
        return 0;
    }

    rep = getrepresentation(idObj, &errmsg);
    if (errmsg)
    {
        Scierror(999, JAVAERROR, fname, errmsg);
        FREE(rep);
        FREE(errmsg);
        return 0;
    }

    err = createMatrixOfString(pvApiCtx, Rhs + 1, 1, 1, (const char* const*) &rep);
    FREE(rep);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    LhsVar(1) = Rhs + 1;

    return 0;
}
/*--------------------------------------------------------------------------*/
