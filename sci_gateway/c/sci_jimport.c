/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "gw_helper.h"
#include "ScilabObjects.h"
#define __USE_DEPRECATED_STACK_FUNCTIONS__
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "getSciArgs.h"
/*--------------------------------------------------------------------------*/
/**
 * Return a _JClass corresponding to given path and return a variable with the same name (if the second var is true).
 * For example, jimport('java.lang.String') will create a variable named String in the stack.
 */
int sci_jimport(char *fname)
{
    SciErr err;
    char *className = NULL;
    int named = 1;
    char *name = NULL;
    char *errmsg = NULL;
    int ret = 0;

    CheckRhs(1, 2);
    CheckLhs(1, 1);

    setCopyOccured(0);
    initialization();
    setIsNew(0);

    className = getSingleString(1, fname);
    if (!className)
    {
        return 0;
    }

    name = strrchr(className, '.');

    if (!name)
    {
        name = className;
    }
    else if (name[1] == '\0')
    {
        Scierror(999, "%s: The class name cannot end with a dot\n", fname);
        freeAllocatedSingleString(className);
        return 0;
    }
    else
    {
        name++;
    }

    if (Rhs == 2)
    {
        int *addr = NULL;
        err = getVarAddressFromPosition(pvApiCtx, 2, &addr);
        if (err.iErr)
        {
            printError(&err, 0);
            return 0;
        }

        if (getScalarBoolean(pvApiCtx, addr, &named))
        {
            return 0;
        }
    }

    ret = loadjavaclass(className, getAllowReload(), &errmsg);

    if (errmsg)
    {
        Scierror(999, JAVAERROR, fname, errmsg);
        FREE(errmsg);
        freeAllocatedSingleString(className);
        return 0;
    }

    if (named)
    {
        createNamedJavaObject(_JCLASS, name, ret);
        freeAllocatedSingleString(className);
        LhsVar(1) = 0;
        PutLhsVar();
        return 0;
    }
    else if (!createJavaObjectAtPos(_JCLASS, Rhs + 1, ret))
    {
        freeAllocatedSingleString(className);
        return 0;
    }

    freeAllocatedSingleString(className);
    LhsVar(1) = Rhs + 1;
    PutLhsVar();

    return 0;
}
/*--------------------------------------------------------------------------*/
