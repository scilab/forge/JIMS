/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "gw_helper.h"
#include "ScilabObjects.h"
#include "noMoreMemory.h"
#define __USE_DEPRECATED_STACK_FUNCTIONS__
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "getSciArgs.h"
#include "MALLOC.h"
/*--------------------------------------------------------------------------*/
int sci_jobj_insert(char *fname)
{
    SciErr err;
    int tmpvarA[2] = {0, 0};
    int tmpvarB[2] = {0, 0};
    int *addr = NULL, *tab = NULL;
    char *errmsg = NULL;
    int idObjA = 0;
    int idObjB = 0;
    int i = 1;
    int type = 0;
    char *fieldName = NULL;

    setCopyOccured(0);
    initialization();
    setIsNew(0);

    err = getVarAddressFromPosition(pvApiCtx, Rhs, &addr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    idObjA = getIdOfArg(addr, fname, tmpvarA, 0, Rhs);
    if (idObjA == -1)
    {
        return 0;
    }

    err = getVarAddressFromPosition(pvApiCtx, Rhs - 1, &addr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    idObjB = getIdOfArg(addr, fname, tmpvarB, 0, Rhs - 1);
    if (idObjB == -1)
    {
        return 0;
    }

    /*
     * If all the parameters are integer, we can guess that a matrix insertion is expected
     */
    tab = (int*)MALLOC(sizeof(int) * (Rhs - 2));
    if (!tab)
    {
        Scierror(999, "%s: %s\n", fname, NOMOREMEMORY);
        return 0;
    }

    for (;i < Rhs - 1; i++)
    {
        err = getVarAddressFromPosition(pvApiCtx, i, &addr);
        if (err.iErr)
        {
            removeTemporaryVars(tmpvarB);
            FREE(tab);
            printError(&err, 0);
            return 0;
        }
        tab[i - 1] = isPositiveIntegerAtAddress(addr);
        if (tab[i - 1] == -1)
        {
            FREE(tab);
            tab = NULL;
            break;
        }
    }

    if (tab)
    {
        setarrayelement(idObjA, tab, Rhs - 2, idObjB, &errmsg);
        FREE(tab);
        removeTemporaryVars(tmpvarB);

        if (errmsg)
        {
            Scierror(999, JAVAERROR, fname, errmsg);
            FREE(errmsg);
            return 0;
        }

        if (!createJavaObjectAtPos(_JOBJ, Rhs + 1, idObjA))
        {
            return 0;
        }

        LhsVar(1) = Rhs + 1;
        PutLhsVar();
        return 0;
    }

    fieldName = getSingleString(1, fname);
    if (!fieldName)
    {
        removeTemporaryVars(tmpvarB);
        return 0;
    }

    type = getfieldtype(idObjA, fieldName, &errmsg);
    if (errmsg)
    {
        Scierror(999, JAVAERROR, fname, errmsg);
        FREE(errmsg);
        removeTemporaryVars(tmpvarB);
        freeAllocatedSingleString(fieldName);
        return 0;
    }

    if (type == 1)
    {
        setfield(idObjA, fieldName, idObjB, &errmsg);
        freeAllocatedSingleString(fieldName);
        removeTemporaryVars(tmpvarB);

        if (errmsg)
        {
            Scierror(999, JAVAERROR, fname, errmsg);
            FREE(errmsg);
            return 0;
        }

        if (!createJavaObjectAtPos(_JOBJ, Rhs + 1, idObjA))
        {
            return 0;
        }

        LhsVar(1) = Rhs + 1;
        PutLhsVar();
        return 0;
    }

    Scierror(999, "%s: No field named %s\n", fname, fieldName);
    freeAllocatedSingleString(fieldName);

    removeTemporaryVars(tmpvarB);
    return 0;
}
/*--------------------------------------------------------------------------*/
