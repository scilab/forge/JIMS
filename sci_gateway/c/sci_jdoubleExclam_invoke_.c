/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#define __USE_DEPRECATED_STACK_FUNCTIONS__
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "getSciArgs.h"
#include "noMoreMemory.h"
#include "gw_helper.h"
/*--------------------------------------------------------------------------*/
/**
 * The function !!_invoke_ is called on object extraction
 */
int sci_jdoubleExclam_invoke_(char *fname)
{
    SciErr err;
    int *addr = NULL;
    int *tmpvar = NULL;
    int *args = NULL;
    int typ = 0;
    int i = 0;
    int len = Rhs;
    char *errmsg = NULL;
    char *kindOfInvocation = NULL;
    int ret = 0;

    CheckLhs(1, 1);

    initialization();

    if (!getCopyOccured()) // if the function is called outside a method context, then return null
    {
        unwrap(0, Rhs + 1);
        LhsVar(1) = Rhs + 1;
        PutLhsVar();

        return 0;
    }
    setCopyOccured(0);

    if (!getMethodName())
    {
        return 0;
    }

    if (len == 1)
    {
        err = getVarAddressFromPosition(pvApiCtx, 1, &addr);
        if (err.iErr)
        {
            printError(&err, 0);
            return 0;
        }
        if (isJVoid(addr))
        {
            len = 0;
        }
    }

    tmpvar = (int*)MALLOC(sizeof(int) * (len + 1));
    if (!tmpvar)
    {
        Scierror(999, "%s: %s\n", fname, NOMOREMEMORY);
        return 0;
    }

    *tmpvar = 0;
    args = (int*)MALLOC(sizeof(int) * len);
    if (!args)
    {
        FREE(tmpvar);
        Scierror(999, "%s: %s\n", fname, NOMOREMEMORY);
        return 0;
    }

    for (; i < len; i++)
    {
        err = getVarAddressFromPosition(pvApiCtx, i + 1, &addr);
        if (err.iErr)
        {
            removeTemporaryVars(tmpvar);
            FREE(args);
            FREE(tmpvar);
            printError(&err, 0);
            return 0;
        }

        args[i] = getIdOfArg(addr, fname, tmpvar, 0, i + 1);
        // If args[i] == -1 then we have a scilab variable which cannot be converted in a Java object.
        if (args[i] == - 1)
        {
            removeTemporaryVars(tmpvar);
            FREE(args);
            FREE(tmpvar);
            return 0;
        }
    }

    if (getIsNew())
    {
        ret = newinstance(getObjectId(), args, len, &errmsg);
        setIsNew(0);
        kindOfInvocation = "Constructor invocation";
    }
    else
    {
        ret = invoke(getObjectId(), getMethodName(), args, len, &errmsg);
        kindOfInvocation = "Method invocation";
    }

    FREE(args);
    freeMethodName();
    removeTemporaryVars(tmpvar);
    FREE(tmpvar);

    if (errmsg)
    {
        Scierror(999, JAVAERROR, kindOfInvocation, errmsg);
        FREE(errmsg);
        return 0;
    }

    if (!getIsNew() && getAutoUnwrap())
    {
        if (!unwrap(ret, Rhs + 1))
        {
            if (!createJavaObjectAtPos(_JOBJ, Rhs + 1, ret))
            {
                removescilabjavaobject(ret);
                return 0;
            }
        }
        else
        {
            removescilabjavaobject(ret);
        }
    }
    else if (!createJavaObjectAtPos(_JOBJ, Rhs + 1, ret))
    {
        removescilabjavaobject(ret);
        return 0;
    }

    setIsNew(0);

    LhsVar(1) = Rhs + 1;
    PutLhsVar();

    return 0;
}
/*--------------------------------------------------------------------------*/
