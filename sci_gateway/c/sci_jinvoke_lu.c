/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "gw_helper.h"
#include "ScilabObjects.h"
#define __USE_DEPRECATED_STACK_FUNCTIONS__
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "noMoreMemory.h"
#include "getSciArgs.h"
/*--------------------------------------------------------------------------*/
/* Function invoke is called with more than 2 arguments : invoke(obj,method,arg1,...,argn)
   - obj is a _JObj mlist so we get his id or can be a String or a number. In this last case, the obj is converted
   in a Java object and the id is got.
   - method is the name of the method.
   - arg1,...,argn are the arguments of the called method, if they're not _JObj mlist, they're converted when it is possible
*/

int sci_jinvoke_lu(char *fname)
{
    SciErr err;
    int typ = 0;
    int *addr = NULL, *listaddr = NULL;
    int len = 0;
    int *tmpvar = NULL;
    int id = 0;
    int *args = NULL;
    int *child = NULL;
    char *methName = NULL;
    int i = 0;
    char *errmsg = NULL;
    int ret = 0;

    CheckRhs(3, 3);

    setCopyOccured(0);
    initialization();
    setIsNew(0);

    err = getVarAddressFromPosition(pvApiCtx, 3, &listaddr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    err = getVarType(pvApiCtx, listaddr, &typ);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    if (typ != sci_list)
    {
        Scierror(999, "%s: Wrong type for input argument %i : List expected\n", fname, 3);
        return NULL;
    }

    err = getListItemNumber(pvApiCtx, listaddr, &len);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    tmpvar = (int*)MALLOC(sizeof(int) * (len + 1));
    if (!tmpvar)
    {
        Scierror(999, "%s: %s\n", fname, NOMOREMEMORY);
        return NULL;
    }
    *tmpvar = 0;

    err = getVarAddressFromPosition(pvApiCtx, 1, &addr);
    if (err.iErr)
    {
        FREE(tmpvar);
        printError(&err, 0);
        return 0;
    }

    if (getScalarInteger32(pvApiCtx, addr, &id))
    {
        FREE(tmpvar);
        return 0;
    }

    args = (int*)MALLOC(sizeof(int) * len);
    if (!args)
    {
        FREE(tmpvar);
        Scierror(999, "%s: %s\n", fname, NOMOREMEMORY);
        return NULL;
    }

    for (; i < len; i++)
    {
        err = getListItemAddress(pvApiCtx, listaddr, i + 1, &child);
        if (err.iErr)
        {
            FREE(args);
            removeTemporaryVars(tmpvar);
            FREE(tmpvar);
            printError(&err, 0);
            return 0;
        }
        args[i] = getIdOfArg(child, fname, tmpvar, 0, i + 1);
        // If args[i] == -1 then we have a scilab variable which cannot be converted in a Java object.
        if (args[i] == - 1)
        {
            FREE(args);
            removeTemporaryVars(tmpvar);
            FREE(tmpvar);
            return 0;
        }
    }

    methName = getSingleString(2, fname);
    if (!methName)
    {
        removeTemporaryVars(tmpvar);
        FREE(tmpvar);
        FREE(args);
        return 0;
    }

    ret = invoke(id, methName, args, len, &errmsg);
    freeAllocatedSingleString(methName);
    FREE(args);

    removeTemporaryVars(tmpvar);
    FREE(tmpvar);

    if (errmsg)
    {
        Scierror(999, JAVAERROR, fname, errmsg);
        FREE(errmsg);
        return 0;
    }

    if (!unwrap(ret, Rhs + 1))
    {
        if (!createJavaObjectAtPos(_JOBJ, Rhs + 1, ret))
        {
            removescilabjavaobject(ret);
            return 0;
        }
    }
    else
    {
        removescilabjavaobject(ret);
    }

    LhsVar(1) = Rhs + 1;
    PutLhsVar();
    return 0;
}
/*--------------------------------------------------------------------------*/
