/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "gw_helper.h"
#include "ScilabObjects.h"
#define __USE_DEPRECATED_STACK_FUNCTIONS__
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "getSciArgs.h"
#include "MALLOC.h"
/*--------------------------------------------------------------------------*/
int sci_jremove(char *fname)
{
    SciErr err;
    int *addr = NULL;
    char *errmsg = NULL;
    int i = 1;

    setCopyOccured(0);
    initialization();
    setIsNew(0);

    if (Rhs == 0)
    {
        garbagecollect(&errmsg);
        if (errmsg)
        {
            Scierror(999, JAVAERROR, fname, errmsg);
            FREE(errmsg);
        }
        LhsVar(1) = 0;
        PutLhsVar();
        return 0;
    }

    for (; i <= Rhs; i++)
    {
        err = getVarAddressFromPosition(pvApiCtx, i, &addr);
        if (err.iErr)
        {
            printError(&err, 0);
            return 0;
        }

        removeVar(fname, addr, i);
    }

    LhsVar(1) = 0;
    PutLhsVar();

    return 0;
}
/*--------------------------------------------------------------------------*/
