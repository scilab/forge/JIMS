/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#define __USE_DEPRECATED_STACK_FUNCTIONS__
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "getSciArgs.h"
#include "MALLOC.h"
#include "gw_helper.h"
/*--------------------------------------------------------------------------*/
int sci_jobj_extract(char *fname)
{
    SciErr err;
    int tmpvar[2] = {0, 0};
    int *addr = NULL;
    int n = -1, *tab = NULL;
    char *errmsg = NULL;
    char *fieldName = NULL;
    int idObj = 0;
    int i = 1;
    int type = 0;

    setCopyOccured(0);
    initialization();
    setIsNew(0);

    err = getVarAddressFromPosition(pvApiCtx, Rhs, &addr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    idObj = getIdOfArg(addr, fname, tmpvar, 0, Rhs);
    if (idObj == -1)
    {
        return 0;
    }

    /*
     * If all the parameters are integer, we can guess that a matrix extraction is expected
     */
    tab = MALLOC(sizeof(int) * (Rhs - 1));

    for (;i < Rhs; i++)
    {
        err = getVarAddressFromPosition(pvApiCtx, i, &addr);
        if (err.iErr)
        {
            FREE(tab);
            printError(&err, 0);
            return 0;
        }
        tab[i - 1] = isPositiveIntegerAtAddress(addr);
        if (tab[i - 1] == -1)
        {
            FREE(tab);
            tab = NULL;
            break;
        }
    }

    if (tab)
    {
        int id = getarrayelement(idObj, tab, Rhs - 1, &errmsg);
        FREE(tab);
        if (errmsg)
        {
            Scierror(999, JAVAERROR, fname, errmsg);
            FREE(errmsg);
            return 0;
        }
        if (!createJavaObjectAtPos(_JOBJ, Rhs + 1, id))
        {
            return 0;
        }

        LhsVar(1) = Rhs + 1;
        PutLhsVar();
        return 0;
    }

    fieldName = getSingleString(1, fname);
    if (!fieldName)
    {
        return 0;
    }

    type = getfieldtype(idObj, fieldName, &errmsg);
    if (errmsg)
    {
        Scierror(999, JAVAERROR, fname, errmsg);
        FREE(errmsg);
        freeAllocatedSingleString(fieldName);
        return 0;
    }

    if (type == 0)
    {
        setMethodName(fieldName);
        setObjectId(idObj);
        copyInvocationMacroToStack(Rhs);
        LhsVar(1) = Rhs;
        PutLhsVar();
        return 0;
    }
    else if (type == 1)
    {
        type = getfield(idObj, fieldName, &errmsg);
        freeAllocatedSingleString(fieldName);

        if (errmsg)
        {
            Scierror(999, JAVAERROR, fname, errmsg);
            FREE(errmsg);
            return 0;
        }

        if (!createJavaObjectAtPos(_JOBJ, Rhs + 1, type))
        {
            return 0;
        }

        LhsVar(1) = Rhs + 1;
        PutLhsVar();
    }
    else
    {
        Scierror(999, "%s: No field or method named %s\n", fname, fieldName);
        freeAllocatedSingleString(fieldName);
    }

    return 0;
}
/*--------------------------------------------------------------------------*/
