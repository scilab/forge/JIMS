/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "gw_helper.h"
#include "ScilabObjects.h"
#define __USE_DEPRECATED_STACK_FUNCTIONS__
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "getSciArgs.h"
/*--------------------------------------------------------------------------*/
int sci_jcast(char *fname)
{
    SciErr err;
    int tmpvar[2] = {0, 0};
    int *addr = NULL;
    int idObj = 0;
    int row = 0, col = 0;
    int *id = NULL;
    char *objName = NULL;
    char *errmsg = NULL;
    int ret = 0;

    CheckRhs(2, 2);

    setCopyOccured(0);
    initialization();
    setIsNew(0);

    err = getVarAddressFromPosition(pvApiCtx, 1, &addr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    idObj = getIdOfArg(addr, fname, tmpvar, 0, 1);
    if (idObj == -1)
    {
        return 0;
    }

    err = getVarAddressFromPosition(pvApiCtx, 2, &addr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    if (isJClass(addr))
    {
        err = getMatrixOfInteger32InList(pvApiCtx, addr, 2, &row, &col, &id);
        if (err.iErr)
        {
            printError(&err, 0);
            return 0;
        }
        ret = javacastwithid(idObj, *id, &errmsg);
    }
    else
    {
        objName = getSingleString(2, fname);
        if (!objName)
        {
            return 0;
        }

        ret = javacast(idObj, objName, &errmsg);
        freeAllocatedSingleString(objName);
    }

    removeTemporaryVars(tmpvar);

    if (errmsg)
    {
        Scierror(999, JAVAERROR, fname, errmsg);
        FREE(errmsg);
        return 0;
    }

    if (!createJavaObjectAtPos(_JOBJ, Rhs + 1, ret))
    {
        removescilabjavaobject(ret);
        return 0;
    }

    LhsVar(1) = Rhs + 1;
    PutLhsVar();
    return 0;
}
/*--------------------------------------------------------------------------*/
