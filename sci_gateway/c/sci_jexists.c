/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#define __USE_DEPRECATED_STACK_FUNCTIONS__
#include "stack-c.h"
#include "api_scilab.h"
#include "OptionsHelper.h"
#include "localization.h"
#include "Scierror.h"
#include "gw_helper.h"
#include "getSciArgs.h"
/*--------------------------------------------------------------------------*/
int sci_jexists(char *fname)
{
    SciErr err;
    int *addr = NULL;
    int *id = NULL;
    int row = 0;
    int col = 0;

    CheckRhs(1, 1);
    CheckLhs(1, 1);

    setCopyOccured(0);
    initialization();
    setIsNew(0);

    err = getVarAddressFromPosition(pvApiCtx, 1, &addr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    if (!isJObj(addr) && !isJClass(addr))
    {
        Scierror(999, "%s: Wrong type for argument 1 : _JObj or _JClass expected\n", fname);
        return 0;
    }

    err = getMatrixOfInteger32InList(pvApiCtx, addr, 2, &row, &col, &id);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    createScalarBoolean(pvApiCtx, 1, isvalidjavaobject(*id));
    LhsVar(1) = 1;
    PutLhsVar();

    return 0;
}
/*--------------------------------------------------------------------------*/
