function build_gateway_c()
  LD_FLAGS = ""

  here = get_absolute_file_path("builder_gateway_c.sce");
  gw_src_c = ["gw_helper.c", ..
  	      "sci_jexists.c", ..
  	      "sci_jenableTrace.c", ..
  	      "sci_jdisableTrace.c", ..
              "sci_jwrapinfloat.c", ..
              "sci_jwrapinchar.c", ..
              "sci_jwrap.c", ..
              "sci_junwraprem.c", ..
              "sci_junwrap.c", ..
              "sci_jsetfield.c", ..
              "sci_jremove.c", ..
              "sci_jobj_print.c", ..
              "sci_jobj_insert.c", ..
              "sci_jobj_extract.c", ..
              "sci_jnewInstance.c", ..
              "sci_jinvoke_lu.c", ..
              "sci_jinvoke_db.c", ..
              "sci_jinvoke.c", ..
              "sci_jinit.c", ..
              "sci_jimport.c", ..
              "sci_jgetrepresentation.c", ..
              "sci_jgetmethods.c", ..
              "sci_jgetfields.c", ..
              "sci_jgetfield.c", ..
              "sci_jgetclassname.c", ..
              "sci_jdeff.c", ..
              "sci_jconvMatrixMethod.c", ..
              "sci_jcompile.c", ..
              "sci_jclass_extract.c", ..
              "sci_jcast.c", ..
              "sci_jautoUnwrap.c", ..
              "sci_jarray.c", ..
              "sci_jallowClassReloading.c", ..
              "sci_jdoubleExclam_invoke_.c", ..
              "sci_displayJObj.c"];

  if getos() == "Windows" then
    gw_src_c = [gw_src_c, ..
                "dllMain.c"];
  end

  CFLAGS = "-I" + fullpath(here + "../../src/include/") + " " + "-I" + here;
  CFLAGS = CFLAGS + " -I" + fullpath(here + "../../src/c/");
  LDFLAGS = "";

  gw_table = ["jdeff" "sci_jdeff";
  	      "jexists" "sci_jexists";
  	      "jenableTrace" "sci_jenableTrace";
  	      "jdisableTrace" "sci_jdisableTrace";
              "jcast" "sci_jcast";
              "jarray" "sci_jarray";
              "jwrap" "sci_jwrap";
              "jwrapinfloat" "sci_jwrapinfloat";
              "jwrapinchar" "sci_jwrapinchar";
              "jconvMatrixMethod" "sci_jconvMatrixMethod";
              "jimport" "sci_jimport";
              "jnewInstance" "sci_jnewInstance";
              "jinvoke" "sci_jinvoke";
              "jinvoke_db" "sci_jinvoke_db";
              "jinvoke_lu" "sci_jinvoke_lu";
              "jsetfield" "sci_jsetfield";
              "jgetfield" "sci_jgetfield";
              "jgetfields" "sci_jgetfields";
              "junwrap" "sci_junwrap";
              "junwraprem" "sci_junwraprem";
              "jautoUnwrap" "sci_jautoUnwrap";
              "jremove" "sci_jremove";
              "jgetmethods" "sci_jgetmethods";
              "jgetrepresentation" "sci_jgetrepresentation";
              "jallowClassReloading" "sci_jallowClassReloading";
              "jgetclassname" "sci_jgetclassname";
              "jinit" "sci_jinit";
              "jcompile" "sci_jcompile";
              "!!_jinvoke_" "sci_jdoubleExclam_invoke_";
              "%_JObj_e" "sci_jobj_extract";
              "%_i__JObj" "sci_jobj_insert";
              "%_JClass_e" "sci_jclass_extract";
              "%_JObj_p" "sci_jobj_print";
              "%_JClass_p" "sci_jobj_print"]

  // PutLhsVar managed by user in sci_sum and in sci_sub
  // if you do not this variable, PutLhsVar is added
  // in gateway generated (default mode in scilab 4.x and 5.x)
  WITHOUT_AUTO_PUTLHSVAR = %t;
  
  tbx_build_gateway("gw_jims_c", gw_table, gw_src_c, here, "", LDFLAGS, CFLAGS);
  
endfunction

build_gateway_c();
clear build_gateway_c;
