<?xml version="1.0" encoding="UTF-8"?>

<!--
*
* JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
* Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
*
* This file must be used under the terms of the CeCILL.
* This source file is licensed as described in the file COPYING, which
* you should have received as part of this distribution.  The terms
* are also available at
* http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
*
*
-->

<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en" xml:id="jconvMatrixMethod">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>
  <refnamediv>
    <refname>jconvMatrixMethod</refname>
    <refpurpose>Set the way to convert matrices</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      method = jconvMatrixMethod()
      jconvMatrixMethod(method)
    </synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>method</term>
        <listitem>
          <para>A string giving the type of conversion: 'rc' or 'cr'</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
      In Scilab a matrix such as <literal>[1 2 3;4 5 6]</literal> is stored <literal>[1 4 2 5 3 6]</literal> (column by column). In Java the same matrix is stored as <literal>[-&gt;[1 2 3] -&gt;[4 5 6]]</literal> (array of pointers).
    </para>
    <para>
      When method is 'cr' (column-row), the internal array <literal>[1 4 2 5 3 6]</literal> is passed as it is and Java creates the array <literal>[-&gt;[1 4] -&gt;[2 5] -&gt;[3 6]]</literal>.
    </para>
    <para>
      When method is 'rc' (row-column), the array <literal>[1 4 2 5 3 6]</literal> is passed as <literal>[1 2 3 4 5 6]</literal> and converted into <literal>[->[1 2 3] ->[4 5 6]]</literal>.
    </para>
    <para>
      By default the method is set to 'cr'.
    </para>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example">
      <![CDATA[
               jconvMatrixMethod();
      ]]></programlisting>
  </refsection>
  <refsection>
    <title>Author</title>
    <simplelist type="vert">
      <member>Calixte Denizet</member>
    </simplelist>
  </refsection>
</refentry>
